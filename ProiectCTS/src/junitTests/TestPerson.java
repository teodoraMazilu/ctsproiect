package junitTests;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import clase.Person;

public class TestPerson extends TestCase {
	
	Person person = null;
	
	@Before
	public void setUp() {
		person = new Person();
	}

	@Test
	public void testUpdate() {
		person.setPersonName("Ana");
		person.setPersonEmail("ana-maria@gmail.com");
		person.setPersonPhoneNumber("0745965231");
		
		String mesaj = "lala";
		
		assertEquals("Ana ai o notificare: " + mesaj, person.update(mesaj));
		assertNotNull(person);
	}
	
	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}
}
