package junitTests;

import java.util.Iterator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import clase.Person;
import clase.Phone;
import clase.Product;
import clase.ProductFactory;
import interfete.Observer;
import junit.framework.TestCase;

public class TestProduct extends TestCase {

	Person p;
	Person p2;
	Product samsung;
	
	@Before
	public void setUp() throws Exception {
		System.out.println("Apel");
		p = new Person("ion",null,"email");
		p2 = new Person("gigel",null,"lalalala");
		samsung = ProductFactory.createProduct("samsung", "telefon", "nu e disponibil");
	}
	
	@Test
	public void testRegisterObserver() {
		samsung.registerObserver(p);
		samsung.registerObserver(p2);
		
		Iterator<Observer> iterator = samsung.getObservers().iterator();
		assertTrue(iterator.hasNext());
		
		assertEquals(samsung.getObservers().size(), 2);
		assertEquals(samsung.getObservers().get(1), p2);
	}
	
	@Test
	public void testRemoveObserver() {
		samsung.registerObserver(p);
		samsung.registerObserver(p2);
		samsung.removeObserver(p);
		
		assertEquals(samsung.getObservers().size(), 1);
	}
	
	@Test
	public void testCreateProduct() {
		Product phone = new Phone("samsung", "nu e disponibil");
		
		//verific daca au aceeasi referinta
		assertNotSame(samsung, phone);
		//verific daca sunt la fel
		//assertEquals(samsung, phone);
		//assertTrue(EqualsBuilder.reflectionEquals(samsung,phone));
	}
	
	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}
}
