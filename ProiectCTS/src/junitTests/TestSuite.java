package junitTests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	TestPerson.class,
	TestProduct.class,
	TestValoareComanda.class,
	TestJunit.class,
	TestComanda.class,
	TestProductFactory.class
})
public class TestSuite {

}
