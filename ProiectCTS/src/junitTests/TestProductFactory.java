package junitTests;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import clase.Product;
import clase.ProductFactory;

public class TestProductFactory extends TestCase{
	
	Product laptop;

	@Before
	public void setUp() throws Exception {
		System.out.println("Test factory method for product.");
		super.setUp();
	}

	@Test
	public void testCreateProduct() {
		String type = null;
		try {
			laptop = ProductFactory.createProduct("", type, "");
			fail("Tip produs=null");
		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Test
	public void testCreateProductCorrectInput() {
		String type = "abcd";
		try {
			laptop = ProductFactory.createProduct("", type, "");
			fail("Tip produs diferit de telefon, laptop sau tableta!");
		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Test
	public void testDescriere() {
		String type = "laptop";
		try {
			laptop = ProductFactory.createProduct("", type, "");
			assertEquals("Verificare metoda descriere laptop", laptop.descriere(), "Este prezent un laptop in comanda");
		} catch(Exception e){
			e.printStackTrace();
		}
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}
}
