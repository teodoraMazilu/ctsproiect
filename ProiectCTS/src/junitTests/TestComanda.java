package junitTests;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import clase.Comanda;
import clase.ComandaEsuata;
import clase.Product;
import clase.ProductFactory;

public class TestComanda extends TestCase{
	
	Comanda comanda;
	Product samsung;
	Comanda comanda2;

	@Before
	public void setUp() throws Exception {
		samsung = ProductFactory.createProduct("samsung", "telefon", "nu e disponibil");
		comanda = new Comanda(1);
		comanda.addProduct(samsung);
	}
	
	@Test
	public void testSetComandaState() {
		comanda.setComandaState();
		assertEquals(comanda.getComandaState().showStatus(), new ComandaEsuata().showStatus());
	}
	
	@Test
	public void testComanda() {
		int numar = -2;
		try{
			comanda2 = new Comanda(numar);
			fail("Numarul comenzii este negativ");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}
}
