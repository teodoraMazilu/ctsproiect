package junitTests;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import clase.Comanda;
import clase.OpValoareFaraTva;
import clase.OpValoareTotala;
import clase.Product;
import clase.ProductFactory;

public class TestValoareComanda extends TestCase{
	
	Comanda comanda;
	Product laptop;
	OpValoareTotala total = new OpValoareTotala();
	OpValoareFaraTva totalFaraTVA = new OpValoareFaraTva();
	
	@Before
	public void setUp() throws Exception {
		System.out.println("Test metode calcul valoare comanda!");
		
		laptop = ProductFactory.createProduct("apple", "laptop", "pe stoc");
		comanda = new Comanda(1);
		laptop.setCantitate(2);
		laptop.setPret(200);
		comanda.addProduct(laptop);
	}

	@Test
	public void testOpValoareTotala() throws Exception {
		float result = total.doOperation(comanda);
		assertEquals(400, result, 0.0);
	}
	
	@Test
	public void testOpValoareTotalaNull() {
		comanda.setProduse(null);
		try{
			float result = total.doOperation(comanda);
			fail("Comanda inexistenta!");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Test
	public void testOpValoareTotalaFaraTvaNull() {
		comanda.setProduse(null);
		try{
			float result = totalFaraTVA.doOperation(comanda);
			fail("Comanda inexistenta!");
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Test
	public void testOpValoareFaraTva() throws Exception {
		float result = totalFaraTVA.doOperation(comanda);
		assertEquals(304, result, 0.0);
	}
	
	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}
}
